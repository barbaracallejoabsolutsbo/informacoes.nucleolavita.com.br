<?php
$title       = "Musicoterapia para idosos em Socorro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A musicoterapia é um trabalho da área da saúde, sem propósitos pedagógicos. Mas não é aula de música, em nosso asilo oferecemos este serviço para todas as pessoas que frequentam nosso espaço, para isso, contamos com profissionais altamente qualificados, sendo assim, iremos realizar esta atividade da maneira correta, levando mais felicidade para estas pessoas.</p>
<p>Conseguindo ofertar Lar para idosos, Clínica para idosos, Dança para idosos, Hotel para idosos com Alzheimer e Diária para idosos a La Vita proporciona Musicoterapia para idosos em Socorro ideal para atender às reais necessidades de seus clientes e parceiros, assim comprovando o fato de ser a empresa mais completa e eficiente do mercado de ASILO. Unindo a experiência que nossa empresa traz com profissionais competentes conseguimos proporcionar o melhor para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>