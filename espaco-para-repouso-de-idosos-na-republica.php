<?php
$title       = "Espaço para repouso de idosos na República";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Um Espaço para repouso de idosos na República é um local de refúgio, sustento ou educação para pessoas com dificuldades de se manter, como toxico dependentes, idosos e órfãos. Oferecer ao idoso a oportunidade de cuidar de sua saúde na presença de profissionais e compartilhar um grande amor com a equipe assistencial pode ser considerado um ato de amor, e irá deixar essa pessoa mais alegre durante os dias.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de ASILO, a La Vita oferece a confiança e a qualidade que você procura quando falamos de Hotel para idosos melhor preço, Lar para idosos preço, Residencial Senior, Preço de casa de repouso para idosos e Creche para idosos. Ainda, com o mais acessível custo x benefício para quem busca Espaço para repouso de idosos na República, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>