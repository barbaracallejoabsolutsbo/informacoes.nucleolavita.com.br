<?php
$title       = "Casa de repouso quanto custa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Um lar de idosos, é um local que possui infraestrutura suficiente para poder promover o bem-estar, a segurança e o cuidado aos idosos. Nossa empresa cuida de cada detalhe no conforto de todas as pessoas que ali se hospedam, contamos com uma equipe altamente qualificada no espaço, entre em contato conosco para saber sobre nossa casa de repouso quanto custa.</p><h2>SAIBA MAIS SOBRE O NOSSO ESPAÇO</h2><p>A casa de repouso,  conta com uma equipe altamente qualificada e especializada que realiza todos os serviços necessários para os idosos, primando pelo total conforto de cada pessoa que nela se hospeda. Para saber mais sobre os valores, condições e especificações da nossa casa, entre em contato agora, teremos todo o gosto em ajudá-lo, e responder a pergunta: casa de repouso quanto custa?</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>