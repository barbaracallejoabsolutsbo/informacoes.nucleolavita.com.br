<?php
$title       = "Hospedagem para idosos no Jardim São Luiz";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Visando proporcionar uma boa hospedagem para os idosos, nosso espaço conta com profissionais de altíssima qualidade, especializados e treinados para proporcionar uma experiência incrível para cada pessoa, além disso, contamos com uma infra-estrutura de altíssima qualidade para que seus entes queridos se sintam bem, por isso, somos a opção certa para seu parente passar dias maravilhosos.</p>
<p>Como uma empresa especializada em ASILO proporcionamos sempre o melhor quando falamos de Hotel para idosos com Alzheimer, Residencial para idosos preço, Clínica para idosos, Clínica de repouso para idosos e Lar para idosos preço. Com potencial necessário para garantir qualidade e excelência em Hospedagem para idosos no Jardim São Luiz com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa La Vita trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>