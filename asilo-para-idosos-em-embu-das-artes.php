<?php
$title       = "Asilo para idosos em Embu das Artes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O asilo para idoso trata-se de um local relacionado à abrigo, ensino, auxílio ou abrigo a uma pessoa de terceira idade, os asilos nos dias de hoje possuem profissionais altamente qualificados e parcerias que promovem uma excelente qualidade de vida dos abrigados. A La Vita possui comodidades altamente prontas e projetadas para atender a todos com a mais alta segurança e conforto, como todas as pessoas merecem.</p>
<p>A La Vita, como uma empresa em constante desenvolvimento quando se trata do mercado de ASILO visa trazer o melhor resultado em Asilo para idosos em Embu das Artes para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Casa de cuidados de idosos, Residencial Senior, Hotel geriátrico, Hotel para idosos e Mensalidade de lar para idosos para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>