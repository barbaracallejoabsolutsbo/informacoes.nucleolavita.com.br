<?php
$title       = "Asilo de luxo em Água Funda";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Asilo de luxo em Água Funda voltado às pessoas idosas auxilia completamente a colocar em outro nível o acolhimento à terceira idade. Nosso espaço concede de uma equipe multidisciplinar com geriatra, enfermeiros, terapeuta ocupacional, nutricionistas, psicólogos, fisioterapeutas e psiquiatras, que acompanham os residentes todos os dias. Venha conhecer o nosso espaço, o melhor de sua região.</p>
<p>Se destacando entre as mais competentes empresas do segmento de ASILO, a La Vita vem proporcionando com total empenho seja em Asilo de luxo em Água Funda quanto em Espaço de cuidados de idosos, Hotel para idosos melhor preço, Asilo de luxo, Clínica de repouso para idosos e Espaço de dança sênior, com o foco em agregando qualidade e excelência em seu atendimento nossa empresa busca a constante melhoria e desenvolvimento de seus colaboradores de forma a garantir o melhor para quem nos busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>