<?php
$title       = "Asilo de luxo na Vila Guilherme";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Asilo de luxo na Vila Guilherme voltado às pessoas idosas auxilia completamente a colocar em outro nível o acolhimento à terceira idade. Nosso espaço concede de uma equipe multidisciplinar com geriatra, enfermeiros, terapeuta ocupacional, nutricionistas, psicólogos, fisioterapeutas e psiquiatras, que acompanham os residentes todos os dias. Venha conhecer o nosso espaço, o melhor de sua região.</p>
<p>Com a La Vita proporcionando o que se tem de melhor e mais moderno no segmento de ASILO consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Residencial para idosos preço, Asilo para idosos, Day care para idosos, Casa de cuidados de idosos e Preço de lares para idosos nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Asilo de luxo na Vila Guilherme.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>