<?php
$title       = "Casa de repouso em família em CECAP - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para ter uma vida muito saudável em qualquer idade, antes de tudo, é extremamente necessário fazer o que traz bem-estar e felicidade. Quer esteja na Casa de repouso em família em CECAP - Guarulhos ou até na sua própria casa, opte sempre pelo conforto e um coração completamente calmo. Entre em contato agora para mais informações. Estamos esperando por você.</p>
<p>Com uma ampla atuação no segmento, a La Vita oferece o melhor quando falamos de Casa de repouso em família em CECAP - Guarulhos proporcionando aos seus clientes a máxima qualidade e desempenho em Hotel para idosos, Residencial Senior, Diária para idosos, Asilo para idosos e Lar para idosos com Alzheimer, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de ASILO.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>