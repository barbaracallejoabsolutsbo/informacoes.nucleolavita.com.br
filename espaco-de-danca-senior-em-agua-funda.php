<?php
$title       = "Espaço de dança sênior em Água Funda";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p> O Espaço de dança sênior em Água Funda é um exercício extremamente comovente que reflete em sua natureza a sensação de bem estar na harmonia de ritmo e melodia, ou, a liberação da alegria de viver, e é uma modalidade da assistência ao idoso que surge como uma opção de trabalho em grupo. Para saber mais sobre a nossa equipe e infraestrutura entre em contato conosco agora mesmo.</p>
<p>Desempenhando uma das melhores assessorias do segmento de ASILO, a La Vita se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Espaço de dança sênior em Água Funda com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Asilo de luxo, Espaço para repouso de idosos, Lar para idosos, Preço de lares para idosos e Hotel para idosos, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>