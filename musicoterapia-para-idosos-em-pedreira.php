<?php
$title       = "Musicoterapia para idosos em Pedreira";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A musicoterapia para pessoas de terceira de idade pode trazer inúmeras vantagens para a saúde além de relaxamento e bem estar, esta técnica terapêutica utiliza a música para tratar transtornos diferentes, entre elas, ansiedade, depressão, transtorno de neurodesenvolvimento, além de ajudar quem tem autismo, TDAH (Transtorno do déficit de atenção com hiperatividade), mal de Parkinson, Alzheimer, entre outros.</p>
<p>A La Vita, como uma empresa em constante desenvolvimento quando se trata do mercado de ASILO visa trazer o melhor resultado em Musicoterapia para idosos em Pedreira para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Lar para idosos preço, Residencial Senior, Cuidados com idosos, Residencial para idosos preço e Clínica de repouso para idosos para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>