<?php
$title       = "Espaço de dança sênior em Capão Redondo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nosso país, um tipo de dança que está conquistando um espaço é a Dança Sênior, que é considerada como um programa para idosos que teve início na Alemanha. E aqui, na La Vita, temos um espaço para os idosos realizarem esta atividade, a dança é geralmente  baseada em músicas folclóricas de diversos povos e, na maioria das vezes, são realizadas em roda, revivendo cantigas e cirandas da infância.</p>
<p>Com sua credibilidade no mercado de ASILO, proporcionando com qualidade, viabilidade e custo x benefício seja em Espaço de dança sênior em Capão Redondo quanto em Recreação para idosos, Casa de cuidados de idosos, Asilo para temporada, Lar para idosos preço e Residencial para idosos preço, podendo dessa forma garantir seu sucesso no segmento em que atua de forma idônea e com altíssimo nível de qualidade a La Vita é a opção número um para você garantir o melhor no que busca!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>