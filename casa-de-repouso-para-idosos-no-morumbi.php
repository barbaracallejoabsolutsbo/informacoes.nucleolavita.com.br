<?php
$title       = "Casa de repouso para idosos no Morumbi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para saber mais sobre como realizamos o serviço de uma Casa de repouso para idosos no Morumbi, entre em contato conosco agora e descubra mais sobre como trabalhamos, nossos preços, missões e objetivos. Marque já a sua visita e explore cada espaço separado que foi feito com o objetivo de ser o melhor espaço para seu parente, garantindo a  segurança, atendimento e total conforto dele. </p>
<p>Com a La Vita proporcionando de forma excelente Rede de lar para idosos, Asilo para idosos, Casa de repouso em família, Especialistas em cuidados com idosos e Asilo para temporada conseguindo manter a alta qualidade e credibilidade no ramo de ASILO, assim, consequentemente, proporcionando o que se tem de melhor em resultados para você. Possibilitando diversas escolhas para os melhores resultados, a nossa empresa torna-se referência com Casa de repouso para idosos no Morumbi.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>