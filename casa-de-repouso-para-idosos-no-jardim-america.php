<?php
$title       = "Casa de repouso para idosos no Jardim América";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para saber mais sobre como realizamos o serviço de uma Casa de repouso para idosos no Jardim América, entre em contato conosco agora e descubra mais sobre como trabalhamos, nossos preços, missões e objetivos. Marque já a sua visita e explore cada espaço separado que foi feito com o objetivo de ser o melhor espaço para seu parente, garantindo a  segurança, atendimento e total conforto dele. </p>
<p>A La Vita, como uma empresa em constante desenvolvimento quando se trata do mercado de ASILO visa trazer o melhor resultado em Casa de repouso para idosos no Jardim América para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Espaço de dança sênior, Clínica de repouso para idosos, Lar para idosos com Alzheimer, Clínica para idosos e Casa de repouso em família para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>