<header itemscope itemtype="http://schema.org/Organization">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N9S77ZZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
    <div class="container">
        <div class="topo">
            <div class="col-md-10">
                <div class="tel">
                    <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
                        <i class="fas fa-phone-alt"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span>
                    </a> 
                    |
                    <a title="whatsApp" href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">
                        <i class="fab fa-whatsapp"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span>
                    </a> 
                    |
                    <a title="E-mail" href="mailto:<?php echo $emailContato; ?>?CC=<?php echo $emailAbsolut; ?>&Subject=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informações%20-%20<?php echo $nome_empresa; ?>">
                        <i class="fas fa-envelope-open-text"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span>
                    </a> 
                </div>
            </div>
            <div class="col-md-2">
                <div class="redes-topo">
                    <a href="" target="_blank" alt="Facebook <?php echo $nome_empresa; ?>" title="Facebook <?php echo $nome_empresa; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a href="" target="_blank" alt="Instagram <?php echo $nome_empresa; ?>" title="Instagram <?php echo $nome_empresa; ?>"><i class="fab fa-instagram"></i></a>
                    <a href="" target="_blank" alt="Youtube <?php echo $nome_empresa; ?>" title="Youtube <?php echo $nome_empresa; ?>"><i class="fab fa-youtube"></i></a>
                    <a href="" target="_blank" alt="Linkedin <?php echo $nome_empresa; ?>" title="Linkedin <?php echo $nome_empresa; ?>"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="col-md-2">
            <div class="logo">
                <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                    <span itemprop="image">
                        <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                    </span>
                </a>
            </div>
        </div>
        
        <div class="col-md-10">
            <nav class="menu">
                <ul class="menu-list">
                    <li><a href="https://nucleolavita.com.br/" title="Página inicial">Home</a></li>
                    <li><a href="https://nucleolavita.com.br/quem-somos" title="Quem Somos">Quem Somos</a></li>
                    <li><a href="https://nucleolavita.com.br/cuidados" title="Cuidados">Cuidados</a></li>
                    <li><a href="https://nucleolavita.com.br/estrutura" title="Estruturas">Estruturas</a></li>
                    <li><a href="<?php echo $url; ?>" title="Informações">Informações</a></li>
                    <li><a href="https://nucleolavita.com.br/contato" title="Contato">Contato</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>