<footer>
    <?php include "includes/btn-fixos.php"; ?>
    
        <div class="footer-copyright">
            <div class="container">
                 <div class="row">
                    <div class="col-md-6">
                        <p>© Núcleo La Vita – Todos os Direitos Reservados</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site"><img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                    <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                    </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es." class="mm-whatsapp" title="Whats App"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>?CC=<?php echo $emailAbsolut; ?>&Subject=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informações%20-%20<?php echo $nome_empresa; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
    </footer>
    <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JFVGNY8J70"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-JFVGNY8J70');
</script>
        <?php } ?>