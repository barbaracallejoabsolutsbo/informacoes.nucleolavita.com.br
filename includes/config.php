<?php

    // Principais Dados do Cliente
$nome_empresa = "La Vita";
$emailContato = "contato@nucleolavita.com.br";

    // Parâmetros de Unidade
$unidades = array(
    1 => array(
        "nome" => "La Vita",
        "rua" => "Rua Gonzaga, 488",
        "bairro" => "Oswaldo Cruz",
        "cidade" => "São Caetano do Sul",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "09540-110",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "2311-5088",
            "whatsapp" => "98965-2636",
            "link_maps" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.444057695066!2d-46.566730085021014!3d-23.62426318465174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5cc4ab786b59%3A0x8d8f201b1dcb65d2!2sR.%20Gonzaga%2C%20488%20-%20Osvaldo%20Cruz%2C%20S%C3%A3o%20Caetano%20do%20Sul%20-%20SP%2C%2009540-110!5e0!3m2!1spt-BR!2sbr!4v1624370661614!5m2!1spt-BR!2sbr" // Incorporar link do maps.google.com
        ),
    2 => array(
        "nome" => "",
        "rua" => "",
        "bairro" => "",
        "cidade" => "",
        "estado" => "",
        "uf" => "",
        "cep" => "",
        "ddd" => "",
        "telefone" => ""
    )
);

    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/informacoes.nucleolavita.com.br/",
        // URL online
    "https://informacoes.nucleolavita.com.br/"
));

    // Variáveis da head.php
$url = $padrao->url;
$canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );

    // Listas de Palavras Chave
    $palavras_chave = array(
        "Asilo de luxo",
        "Asilo para temporada",
        "Asilo para idosos",
        "Casa de cuidados de idosos",
        "Casa de repouso em família",
        "Casa de repouso geriátrica",
        "Casa de repouso para idosos",
        "Casa de repouso quanto custa",
        "Clínica de repouso para idosos",
        "Clínica para idosos",
        "Creche para idosos",
        "Cuidados com idosos",
        "Dança para idosos",
        "Day care para idosos",
        "Diária para idosos",
        "Espaço de cuidados de idosos",
        "Espaço de dança sênior",
        "Espaço para repouso de idosos",
        "Especialistas em cuidados com idosos",
        "Hospedagem para idosos",
        "Hotel para idosos",
        "Hotel para idosos melhor preço",
        "Hotel geriátrico",
        "Hotel para idosos com Alzheimer",
        "Lar de idosos de alto padrão",
        "Lar para idosos",
        "Lar para idosos com Alzheimer",
        "Lar para idosos preço",
        "Mensalidade de lar para idosos",
        "Moradia para idosos",
        "Musicoterapia para idosos",
        "Preço de casa de repouso para idosos",
        "Preço de lares para idosos",
        "Preço de residenciais para idosos",
        "Rede de lar para idosos",
        "Recreação para idosos",
        "Residencial para idosos",
        "Residencial para idosos preço",
        "Valor de asilo para idosos",
        "Residencial Senior",
        "Residencial Senior preço"
    );

    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */