<?php
$title       = "Lar de idosos de alto padrão no Pacaembú";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Há alguns anos, as principais casas de repouso da capital eram cortiços com um quintal muito apertado e quartos compartilhados, onde várias camas ficavam uma ao lado da outra, hoje tudo mudou e você pode encontrar um Lar de idosos de alto padrão no Pacaembú em muitos lugares o que proporciona uma acessibilidade muito melhor e maior para que as pessoas mais velhas se sintam completamente confortáveis.</p>
<p>Se está procurando por Lar de idosos de alto padrão no Pacaembú e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a La Vita é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de ASILO conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Clínica de repouso para idosos, Clínica para idosos, Residencial para idosos, Hotel para idosos com Alzheimer e Creche para idosos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>