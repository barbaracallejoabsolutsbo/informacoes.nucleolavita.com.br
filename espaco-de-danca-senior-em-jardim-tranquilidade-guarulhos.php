<?php
$title       = "Espaço de dança sênior em Jardim Tranquilidade - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nosso país, um tipo de dança que está conquistando um espaço é a Dança Sênior, que é considerada como um programa para idosos que teve início na Alemanha. E aqui, na La Vita, temos um espaço para os idosos realizarem esta atividade, a dança é geralmente  baseada em músicas folclóricas de diversos povos e, na maioria das vezes, são realizadas em roda, revivendo cantigas e cirandas da infância.</p>
<p>A empresa La Vita é destaque entre as principais empresas do ramo de ASILO, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Espaço de dança sênior em Jardim Tranquilidade - Guarulhos do mercado. Ainda, possui facilidade com Lar para idosos com Alzheimer, Lar de idosos de alto padrão, Preço de casa de repouso para idosos, Residencial para idosos preço e Preço de lares para idosos mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>