<?php
$title       = "Asilo para idosos em Campo Belo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Disponibilizar ao idoso a chance de cuidar de seu bem-estar no Asilo para idosos em Campo Belo com profissionais por perto e dividir de muito amor pela nossa equipe, pode ser considerado uma demonstração de amor. Nosso espaço oferece de uma equipe multidisciplinar com geriatra, enfermeiros, terapeutas ocupacionais e diversos outros profissionais, que são qualificados para a realização deste serviço da maneira correta. Faça uma visita no melhor asilo.</p>
<p>Desempenhando uma das melhores assessorias do segmento de ASILO, a La Vita se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Asilo para idosos em Campo Belo com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Lar para idosos preço, Casa de repouso geriátrica, Asilo para temporada, Lar para idosos com Alzheimer e Asilo para idosos, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>