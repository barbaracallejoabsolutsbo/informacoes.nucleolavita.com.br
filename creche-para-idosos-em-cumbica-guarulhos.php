<?php
$title       = "Creche para idosos em Cumbica - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Creche para idosos em Cumbica - Guarulhos é uma ótima solução para aquelas famílias que não querem perder o contato diário com um idoso, como pode ser o caso de asilos, a grande ideia da creche é que os idosos se sentem bem-vindos e podem fazer o que quiserem. Entre em contato com nossa equipe para saber mais sobre nossos serviços, o nosso espaço é o melhor para seu parente passar um tempo.</p>
<p>Especialista no mercado, a La Vita é uma empresa que ganha visibilidade quando se trata de Creche para idosos em Cumbica - Guarulhos, já que possui mão de obra especializada em Rede de lar para idosos, Asilo de luxo, Preço de casa de repouso para idosos, Mensalidade de lar para idosos e Dança para idosos. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de ASILO, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>