<?php
$title       = "Asilo para temporada em Campo Belo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está à procura do melhor Asilo para temporada em Campo Belo que de fato se preocupe com as pessoas de terceira idade, você certamente está no lugar certo. Nós da La Vita atendemos a todos com a maior atenção e determinação, desejando ser modelo em todo o território nacional, para isso, nossa empresa possui comodidades altamente realizadas e projetadas para receber a todos com a mais alta segurança e bem- estar.</p>
<p>Nós da La Vita trabalhamos dia a dia para garantir o melhor em Asilo para temporada em Campo Belo e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de ASILO com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Preço de casa de repouso para idosos, Asilo para temporada, Hotel para idosos melhor preço, Casa de repouso geriátrica e Recreação para idosos e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>