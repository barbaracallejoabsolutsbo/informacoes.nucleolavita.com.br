<?php
$title       = "Asilo para idosos no Pacaembú";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Disponibilizar ao idoso a chance de cuidar de seu bem-estar no Asilo para idosos no Pacaembú com profissionais por perto e dividir de muito amor pela nossa equipe, pode ser considerado uma demonstração de amor. Nosso espaço oferece de uma equipe multidisciplinar com geriatra, enfermeiros, terapeutas ocupacionais e diversos outros profissionais, que são qualificados para a realização deste serviço da maneira correta. Faça uma visita no melhor asilo.</p>
<p>Além de sermos uma empresa especializada em Asilo para idosos no Pacaembú disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Residencial Senior, Casa de repouso em família, Musicoterapia para idosos, Hotel para idosos melhor preço e Espaço de cuidados de idosos. Com a ampla experiência que a equipe La Vita possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de ASILO.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>