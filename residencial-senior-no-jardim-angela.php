<?php
$title       = "Residencial Senior no Jardim Ângela";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O diferencial do Residencial Senior no Jardim Ângela é que vai além dos cuidados básicos, como higiene pessoal, alimentação e assistência médica, e um dos grandes objetivos é proporcionar exatamente uma extensão do ambiente familiar, proporcionar apoio emocional e desenvolver uma grande autonomia em relação à rotina do morador, fugindo do padrão das casas de repouso tradicionais.</p>
<p>Se está procurando por Residencial Senior no Jardim Ângela e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a La Vita é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de ASILO conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Hotel para idosos, Hotel para idosos com Alzheimer, Casa de cuidados de idosos, Recreação para idosos e Preço de lares para idosos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>