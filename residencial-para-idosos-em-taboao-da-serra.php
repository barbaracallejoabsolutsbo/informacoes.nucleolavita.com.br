<?php
$title       = "Residencial para idosos em Taboão da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está a procura de um Residencial para idosos em Taboão da Serra que realmente se preocupe com os idosos você certamente está no lugar certo. Nós da La Vita atendemos a todos com o maior cuidado e segurança, visando ser referência para todo o Brasil. Nossa empresa possui comodidades altamente prontas e projetadas para atender a todos com a mais alta segurança e conforto.</p>
<p>Se deseja comprar Residencial para idosos em Taboão da Serra e procura por uma empresa séria e competente, a La Vita é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Moradia para idosos, Casa de cuidados de idosos, Espaço para repouso de idosos, Preço de residenciais para idosos e Diária para idosos. Entre em contato e saiba tudo sobre ASILO com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>