<?php
$title       = "Residencial Senior em Cajamar";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O diferencial do Residencial Senior em Cajamar é que vai além dos cuidados básicos, como higiene pessoal, alimentação e assistência médica, e um dos grandes objetivos é proporcionar exatamente uma extensão do ambiente familiar, proporcionar apoio emocional e desenvolver uma grande autonomia em relação à rotina do morador, fugindo do padrão das casas de repouso tradicionais.</p>
<p>Procurando uma empresa de confiança onde você possa garantir o melhor em ASILO, a La Vita é a companhia certa. Aqui desde Residencial Senior em Cajamar até Residencial para idosos, Hospedagem para idosos, Musicoterapia para idosos, Asilo para temporada e Mensalidade de lar para idosos é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções em energia sustentável. Entre em contato conosco e saiba mais!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>