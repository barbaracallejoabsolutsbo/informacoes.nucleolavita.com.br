<?php
$title       = "Especialistas em cuidados com idosos no Ipiranga";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Pelo preço das residências para idosos oferecido em São Paulo, o La Vita atende melhor os critérios de custo-benefício. Nossa unidade oferece instalações de alto padrão e trabalha com os mais renomados especialistas em cuidados com  idosos geriatras e demais médicos, para saber mais sobre nossa equipe e infraestrutura, entre em contato agora com o melhor asilo de sua região.</p>
<p>Desempenhando uma das melhores assessorias do segmento de ASILO, a La Vita se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Especialistas em cuidados com idosos no Ipiranga com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Residencial para idosos, Hotel geriátrico, Rede de lar para idosos, Lar para idosos e Residencial para idosos preço, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>