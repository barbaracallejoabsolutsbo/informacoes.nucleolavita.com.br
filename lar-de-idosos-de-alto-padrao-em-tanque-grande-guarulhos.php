<?php
$title       = "Lar de idosos de alto padrão em Tanque Grande - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Um Lar de idosos de alto padrão em Tanque Grande - Guarulhos voltado para os idosos ajuda completamente a cuidar dessas pessoas a outro nível. O espaço conta com uma equipe multidisciplinar composta por geriatras, enfermeiras, terapeutas ocupacionais, nutricionistas, psicólogos, fisioterapeutas e psiquiatras que acompanham diariamente os residentes. Venha conhecer o nosso espaço.</p>
<p>Como uma empresa especializada em ASILO proporcionamos sempre o melhor quando falamos de Cuidados com idosos, Recreação para idosos, Day care para idosos, Clínica de repouso para idosos e Casa de repouso para idosos. Com potencial necessário para garantir qualidade e excelência em Lar de idosos de alto padrão em Tanque Grande - Guarulhos com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa La Vita trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>